
module.exports = function ( karma ) {
  // process.env.PHANTOMJS_BIN = 'node_modules/phantomjs/bin/phantomjs';

  karma.set({
    /**
     * From where to look for files, starting with the location of this file.
     */
    basePath: './',
	//  logLevel: 'verbose',
	 logLevel: 'info',
    /**
     * Filled by the task `gulp karma-conf`
     */
    files: [
                                  'bower_components/jquery/dist/jquery.js',
                                  'bower_components/lodash/lodash.js',
                                  'bower_components/angular/angular.js',
                                  'bower_components/angular-animate/angular-animate.js',
                                  'bower_components/angular-loading-bar/build/loading-bar.js',
                                  'bower_components/angular-ui-router/release/angular-ui-router.js',
                                  'bower_components/angular-ui-bootstrap-bower/ui-bootstrap-tpls.js',
                                  'bower_components/angular-ui-utils/ui-utils.js',
                                  'bower_components/moment/moment.js',
                                  'bower_components/angular-bootstrap-show-errors/src/showErrors.js',
                                  'bower_components/angular-sanitize/angular-sanitize.js',
                                  'bower_components/angular-xeditable/dist/js/xeditable.js',
                                  'bower_components/angular-toastr/dist/angular-toastr.tpls.js',
                                  'bower_components/rangy/rangy-core.js',
                                  'bower_components/rangy/rangy-classapplier.js',
                                  'bower_components/rangy/rangy-highlighter.js',
                                  'bower_components/rangy/rangy-selectionsaverestore.js',
                                  'bower_components/rangy/rangy-serializer.js',
                                  'bower_components/rangy/rangy-textrange.js',
                                  'bower_components/bootstrap/dist/js/bootstrap.js',
                                  'bower_components/sails.io.js/dist/sails.io.js',
                                  'bower_components/angularSails/dist/ngsails.io.js',
                                  'bower_components/ngstorage/ngStorage.js',
                                  'bower_components/highcharts/highcharts.js',
                                  'bower_components/highcharts/highcharts-more.js',
                                  'bower_components/highcharts/modules/exporting.js',
                                  'bower_components/highcharts-ng/dist/highcharts-ng.js',
                                  'bower_components/angular-moment/angular-moment.js',
                                  'bower_components/textAngular/src/textAngular.js',
                                  'bower_components/textAngular/src/textAngular-sanitize.js',
                                  'bower_components/textAngular/src/textAngularSetup.js',
                                  'bower_components/bootbox/bootbox.js',
                                  'bower_components/ngBootbox/dist/ngBootbox.js',
                                  'bower_components/angular-mocks/angular-mocks.js',
                                  'app/core/layout/partials/navigation.partials.js',
                                  'app/core/layout/partials/help.partials.js',
                                  'app/core/layout/partials/header.partials.js',
                                  'app/core/layout/partials/footer.partials.js',
                                  'app/core/layout/partials/files.partials.js',
                                  'app/core/error/partials/error.partials.js',
                                  'app/core/directives/partials/ListSearch.partials.js',
                                  'app/core/auth/services/services.js',
                                  'app/core/auth/services/UserService.js',
                                  'app/core/auth/services/AuthService.js',
                                  'app/core/auth/login/login.partials.js',
                                  'app/core/auth/login/login.js',
                                  'app/core/auth/login/login-controllers.js',
                                  'app/examples/messages/messages.partials.js',
                                  'app/examples/messages/messages.js',
                                  'app/examples/messages/messages-info.partials.js',
                                  'app/examples/messages/messages-controller.js',
                                  'app/examples/chat/chat.partials.js',
                                  'app/examples/chat/chat.js',
                                  'app/examples/chat/chat-models.js',
                                  'app/examples/chat/chat-info.partials.js',
                                  'app/examples/chat/chat-directives.js',
                                  'app/examples/chat/chat-controllers.js',
                                  'app/examples/book/list.partials.js',
                                  'app/examples/book/list-info.partials.js',
                                  'app/examples/book/book.partials.js',
                                  'app/examples/book/book.js',
                                  'app/examples/book/book-models.js',
                                  'app/examples/book/book-controllers.js',
                                  'app/examples/book/add.partials.js',
                                  'app/examples/author/list.partials.js',
                                  'app/examples/author/list-info.partials.js',
                                  'app/examples/author/author.partials.js',
                                  'app/examples/author/author.js',
                                  'app/examples/author/author-models.js',
                                  'app/examples/author/author-controllers.js',
                                  'app/examples/author/add.partials.js',
                                  'app/examples/about/about.partials.js',
                                  'app/examples/about/about.js',
                                  'app/core/services/services.js',
                                  'app/core/services/SocketHelperService.js',
                                  'app/core/services/MessageService.js',
                                  'app/core/services/ListConfigService.js',
                                  'app/core/services/HttpStatusService.js',
                                  'app/core/services/DataService.js',
                                  'app/core/models/models.js',
                                  'app/core/models/DataModel.js',
                                  'app/core/libraries/libraries.js',
                                  'app/core/libraries/LoDash.js',
                                  'app/core/layout/layout.js',
                                  'app/core/layout/layout-services.js',
                                  'app/core/layout/layout-directives.js',
                                  'app/core/layout/layout-controllers.js',
                                  'app/core/interceptors/interceptors.js',
                                  'app/core/interceptors/ErrorInterceptor.js',
                                  'app/core/interceptors/AuthInterceptor.js',
                                  'app/core/filters/filters.js',
                                  'app/core/error/error.js',
                                  'app/core/error/error-controllers.js',
                                  'app/core/directives/directives.js',
                                  'app/core/directives/ListSearch.js',
                                  'app/core/dependencies/dependencies.js',
                                  'app/app.js',
                                  'app/core/constants/BackendConfig.js',
                                  'app/core/constants/AccessLevels.js',
                                  'app/core/components/components.js',
                                  'app/core/components/FocusOn.js',
                                  'app/core/auth/auth.js',
                                  'app/admin/loginHistory/login-history.js',
                                  'app/admin/loginHistory/login-history-models.js',
                                  'app/admin/loginHistory/login-history-controllers.js',
                                  'app/admin/loginHistory/index.partials.js',
                                  'app/examples/examples.js',
                                  'app/core/core.js',
                                  'app/admin/admin.js',
                                  'src/app/examples/chat/chat-controllers_test.js'
                                ],

	 frameworks: [ 'mocha', 'chai', 'sinon-chai' ],
    plugins: [ 'karma-mocha', 'karma-mocha-reporter', 'karma-chai', 'karma-sinon-chai', 'karma-phantomjs-launcher' ],

    /**
     * How to report, by default.
     */
 	reporters: 'mocha',

    /**
     * Show colors in output?
     */
    colors: true,

    /**
     * On which port should the browser connect, on which port is the test runner
     * operating, and what is the URL path for the browser to use.
     */
    port: 9099,
    runnerPort: 9100,
    urlRoot: '/',

    /**
     * Disable file watching by default.
     */
    autoWatch: false,

    /**
     * The list of browsers to launch to test on. This includes only "Firefox" by
     * default, but other browser names include:
     * Chrome, ChromeCanary, Firefox, Opera, Safari, PhantomJS
     */
    browsers: [
      'PhantomJS'
    ]
  });
};
