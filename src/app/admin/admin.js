(function() {
  'use strict';

  angular.module('frontend.admin', [
    'frontend.admin.login-history'
  ]);

  angular.module('frontend.admin').config(
      function config($stateProvider) {
        $stateProvider.state('admin', {
            parent: 'frontend',
            data: { access: 2 },
            views: {
              'content@': {
                controller: function($state) {
                    $state.go('admin.login-history');
                  }
              },
              'pageNavigation@': {
                templateUrl: '/frontend/core/layout/partials/navigation.html',
                controller: 'NavigationController',
                resolve: {
                  _items: function resolve(ContentNavigationItems) {
                      return ContentNavigationItems.getItems('admin');
                    }
                }
              }
            }
          });
      }
   );
}());
