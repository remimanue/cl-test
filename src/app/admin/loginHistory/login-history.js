(function() {
  'use strict';

  angular.module('frontend.admin.login-history', []);
  angular.module('frontend.admin.login-history').config(
      function config($stateProvider) {
        $stateProvider.state('admin.login-history', {
            url: '/admin/loginHistory',
            views: {
              'content@': {
                templateUrl: '/frontend/admin/loginHistory/index.html',
                controller: 'LoginHistoryController',
                resolve: {
                  _items: function resolve(ListConfig, LoginHistoryModel) {
                      var config = ListConfig.getConfig();
                      var parameters = {
                        limit: config.itemsPerPage,
                        sort: 'createdAt DESC',
                        populate: 'user'
                      };
                      return LoginHistoryModel.load(parameters);
                  },
                  _count: function resolve(LoginHistoryModel) {
                      return LoginHistoryModel.count();
                  },
                  _statsBrowser: function resolve(LoginHistoryModel) {
                      return LoginHistoryModel.statistics('Browser');
                  },
                  _statsOS: function resolve(LoginHistoryModel) {
                      return LoginHistoryModel.statistics('OS');
                  },
                  _statsUser: function resolve(LoginHistoryModel) {
                      return LoginHistoryModel.statistics('User');
                  }
                }
              }
            }
          });
      }
    );
}());
