(function () {
  'use strict';

  angular.module('frontend.admin.login-history').factory('LoginHistoryModel',
      function factory($log, DataModel, DataService) {
        var model = new DataModel('userlogin');
        model.statistics = function statistics(type) {
          var self = this;
          return DataService
            .collection(self.endpoint + '/statistics/', {type: type})
            .then(
              function onSuccess(response) {
                return response.data;
              },
              function onError(error) {
                $log.error('LoginHistoryModel.statistics() failed.', error, self.endpoint, type);
              }
            );
        };
        return model;
      }
    );
}());
