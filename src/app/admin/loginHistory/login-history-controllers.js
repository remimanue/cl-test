(function() {
  'use strict';

  angular.module('frontend.admin.login-history')
    .controller('LoginHistoryController',
      function controller($scope, $timeout, $q, $filter, _,
        ListConfig, SocketHelperService, LoginHistoryModel,
        _items, _count, _statsBrowser, _statsOS, _statsUser) {

        LoginHistoryModel.setScope($scope, false, 'items', 'itemCount');
        $scope.statsBrowser = _statsBrowser;
        $scope.statsOS = _statsOS;
        $scope.statsUser = _statsUser;
        $scope = angular.extend($scope, angular.copy(ListConfig.getConfig()));
        $scope.items = _items;
        $scope.itemCount = _count.count;
        $scope.titleItems = ListConfig.getTitleItems(LoginHistoryModel.endpoint);
        $scope.sort = {
          column: 'createdAt',
          direction: false
        };
        $scope.filters = {
          searchWord: '',
          columns: $scope.titleItems
        };
        var chartConfig = {
          options: {
            chart: {
              type: 'pie'
            },
            plotOptions: {
              pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                  enabled: false
                },
                showInLegend: true
              }
            },
            exporting: {
              enabled: false
            },
            tooltip: {
              formatter: function formatter() {
                return '<strong>' + this.key + '</strong><br />' +
                  'Percentage: ' + $filter('number')(this.point.percentage, 2) + '%<br />' +
                  'Total: ' + $filter('number')(this.y)
                ;
              },
              pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            }
          },
          title: {
            text: ''
          },
          series: [{
            type: 'pie',
            name: '',
            data: []
          }]
        };
        var charts = [{
            scope: 'chartBrowser',
            data: 'statsBrowser',
            title: 'Browsers'
          }, {
            scope: 'chartOs',
            data: 'statsOS',
            title: 'Operating systems'
          }, {
            scope: 'chartUser',
            data: 'statsUser',
            title: 'Users'
      	}];
        _.forEach(charts, function iterator(config) {
          $scope[config.scope] = angular.copy(chartConfig);
          $scope[config.scope].series[0].data = $scope[config.data];
          $scope[config.scope].title.text = config.title;
        });

        $scope.changeSort = function changeSort(item) {
          var sort = $scope.sort;
          if (sort.column === item.column) {
            sort.direction = !sort.direction;
          } else {
            sort.column = item.column;
            sort.direction = true;
          }
          _triggerFetchData();
        };

        $scope.$watch('items', function watcher(valueNew, valueOld) {
          if (valueNew !== valueOld) {
            var actions = [{
                action: 'Browser',
                scope: 'chartBrowser'
              }, {
                action: 'OS',
                scope: 'chartOs'
              }, {
                action: 'User',
                scope: 'chartUser'
              }];
            var promises = _.map(actions, function iterator(config) {
              LoginHistoryModel.statistics(config.action)
                .then(
                  function onSuccess(data) {
                    $scope[config.scope].series[0].data = data;
                  }
                )
              ;
            });
            $q.all(promises);
          }
        });

        $scope.$watch('currentPage', function watcher(valueNew, valueOld) {
          if (valueNew !== valueOld) {
            _fetchData();
          }
        });
        $scope.$watch('itemsPerPage', function watcher(valueNew, valueOld) {
          if (valueNew !== valueOld) {
            _triggerFetchData();
          }
        });

        var searchWordTimer;
        $scope.$watch('filters', function watcher(valueNew, valueOld) {
          if (valueNew !== valueOld) {
            if (searchWordTimer) {
              $timeout.cancel(searchWordTimer);
            }

            searchWordTimer = $timeout(_triggerFetchData, 400);
          }
        }, true);

        function _triggerFetchData() {
          if ($scope.currentPage === 1) {
            _fetchData();
          } else {
            $scope.currentPage = 1;
          }
        }

        function _fetchData() {
          $scope.loading = true;
          // Common parameters for count and data query
          var commonParameters = {
            where: SocketHelperService.getWhere($scope.filters),
            populate: 'user'
          };
          // Data query specified parameters
          var parameters = {
            limit: $scope.itemsPerPage,
            skip: ($scope.currentPage - 1) * $scope.itemsPerPage,
            sort: $scope.sort.column + ' ' + ($scope.sort.direction ? 'ASC' : 'DESC')
          };
          // Fetch data count
          var count = LoginHistoryModel
            .count(commonParameters)
            .then(
              function onSuccess(response) {
                $scope.itemCount = response.count;
              }
            );
          // Fetch actual data
          var load = LoginHistoryModel
            .load(_.merge({}, commonParameters, parameters))
            .then(
              function onSuccess(response) {
                $scope.items = response;
              }
            );
          // Load all needed data
          $q.all([count, load]).finally(
              function onFinally() {
                $scope.loaded = true;
                $scope.loading = false;
              }
            );
        }
      }
    );
}());
