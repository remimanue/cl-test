(function() {
  'use strict';

  angular.module('frontend.core.components').directive('focusOn',
      function directive($timeout) {
        return function focusOn(scope, element) {
          scope.$on('focusOn', function focusOnEvent(event, identifier) {
            if (element.data('focusOn') && identifier === element.data('focusOn')) {
              $timeout(function timeout() {
                element.focus();
              }, 0, false);
            }
          });
        };
      }
    );

  angular.module('frontend.core.components').factory('FocusOnService',
      function factory($rootScope, $timeout) {
        return {
          'focus': function focus(identifier) {
            $timeout(function timeout() {
              $rootScope.$broadcast('focusOn', identifier);
            }, 0, false);
          }
        };
      }
    );
}());
