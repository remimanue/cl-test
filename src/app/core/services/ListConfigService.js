(function() {
  'use strict';

  angular.module('frontend.core.services').factory('ListConfig',
      function factory(_) {
        var titleItems = {
          author: [{
              title: 'Utilisateur',
              column: 'name',
              class: 'col-xs-11',
              searchable: true,
              sortable: true,
              inSearch: true,
              inTitle: true
            }, {
              title: 'Documents',
              column: false,
              class: 'text-right col-xs-1',
              searchable: false,
              sortable: false,
              inSearch: false,
              inTitle: true
         }],
          book: [{
              title: 'Titre',
              column: 'title',
              class: 'col-xs-8',
              searchable: true,
              sortable: true,
              inSearch: true,
              inTitle: true
            }, {
              title: 'Utilisateur',
              column: false,
              class: 'col-xs-3',
              searchable: false,
              sortable: false,
              inSearch: false,
              inTitle: true
            }, {
              title: 'Année',
              column: 'releaseDate',
              class: 'col-xs-1 text-right',
              searchable: true,
              sortable: true,
              inSearch: true,
              inTitle: true
            }],
          userlogin: [{
              title: 'IP-address',
              column: 'ip',
              class: 'col-xs-2',
              searchable: true,
              sortable: true,
              inSearch: true,
              inTitle: true
            }, {
              title: 'Browser',
              column: 'browser',
              class: 'col-xs-2',
              searchable: true,
              sortable: true,
              inSearch: true,
              inTitle: true
            }, {
              title: 'Operating System',
              column: 'os',
              class: 'col-xs-2',
              searchable: true,
              sortable: true,
              inSearch: true,
              inTitle: true
            }, {
              title: 'Username',
              column: false,
              class: 'col-xs-2',
              searchable: false,
              sortable: false,
              inSearch: false,
              inTitle: true
            }, {
              title: 'Login time',
              column: 'createdAt',
              class: 'col-xs-4',
              searchable: false,
              sortable: true,
              inSearch: false,
              inTitle: true
            }]
        };
        return {
          /** @returns {{
           *            itemCount:            Number,
           *            items:                Array,
           *            itemsPerPage:         Number,
           *            itemsPerPageOptions:  Array,
           *            currentPage:          Number,
           *            where:                {},
           *            loading:              Boolean,
           *            loaded:               Boolean
           *          }} */
          getConfig: function getConfig() {
            return {
              itemCount: 0,
              items: [],
              itemsPerPage: 10,
              itemsPerPageOptions: [10, 25, 50, 100],
              currentPage: 1,
              where: {},
              loading: true,
              loaded: false
            };
          },
          getTitleItems: function getTitleItems(model) {
            return _.isUndefined(titleItems[model]) ? [] : titleItems[model];
          }
        };
      }
    );
}());
