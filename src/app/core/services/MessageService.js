(function() {
  'use strict';

  angular.module('frontend.core.services').factory('MessageService',
      function factory(toastr, _) {
        var service = {};
        function _makeMessage(message, title, options, defaultOptions, type) {
          title = title || '';
          options = options || {};
          toastr[type](message, title, _.assign(defaultOptions, options));
        }
        service.success = function success(message, title, options) {
          var defaultOptions = { timeOut: 2000 };
          _makeMessage(message, title, options, defaultOptions, 'success');
        };
        service.info = function error(message, title, options) {
          var defaultOptions = { timeout: 3000 };
          _makeMessage(message, title, options, defaultOptions, 'info');
        };
        service.warning = function error(message, title, options) {
          var defaultOptions = { timeout: 3000 };
          _makeMessage(message, title, options, defaultOptions, 'warning');
        };
        service.error = function error(message, title, options) {
          var defaultOptions = { timeout: 4000 };
          _makeMessage(message, title, options, defaultOptions, 'error');
        };
        return service;
      }
    );
}());
