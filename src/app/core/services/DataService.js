(function() {
  'use strict';

  angular.module('frontend.core.services').factory('DataService',
      function factory($sailsSocket, _, BackendConfig) {
        function _parseEndPointUrl(endPoint, identifier) {
          if (!_.isUndefined(identifier)) {
            endPoint = endPoint + '/' + identifier;
          }
          return BackendConfig.url + '/' + endPoint;
        }
        function _parseParameters(parameters) {
          parameters = parameters || {};
          return {params: parameters};
        }
        return {
          count: function count(endPoint, parameters) {
            return $sailsSocket
              .get(_parseEndPointUrl(endPoint) + '/count/', _parseParameters(parameters));
          },
          collection: function collection(endPoint, parameters) {
            return $sailsSocket
              .get(_parseEndPointUrl(endPoint), _parseParameters(parameters));
          },
          fetch: function fetch(endPoint, identifier, parameters) {
            return $sailsSocket
              .get(_parseEndPointUrl(endPoint, identifier), _parseParameters(parameters));
          },
          create: function create(endPoint, data) {
            return $sailsSocket
              .post(_parseEndPointUrl(endPoint), data);
          },
          update: function update(endPoint, identifier, data) {
            return $sailsSocket
              .put(_parseEndPointUrl(endPoint, identifier), data);
          },
          delete: function remove(endPoint, identifier) {
            return $sailsSocket
              .delete(_parseEndPointUrl(endPoint, identifier));
          }
        };
      }
    );
}());
