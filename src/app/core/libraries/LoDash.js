(function() {
  'use strict';

  angular.module('frontend.core.libraries')
    .factory('_', function factory($window) {
        return $window._;
      }
    );
}());
