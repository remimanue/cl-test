(function() {
  'use strict';

  angular.module('frontend.core.models').factory('DataModel',
      function($sailsSocket, $log, _, DataService) {
        var DataModel = function(endpoint) {
          // Initialize default values.
          this.object = {};
          this.objects = [];
          // Cache parameters
          this.cache = {
            count: {},
            fetch: {},
            load: {}
          };
          // Is scope used with data model or not, if yes this is actual scope
          this.scope = false;
          // Scope item names for single, collection and count
          this.itemNames = {
            object: false,
            objects: false,
            count: false
          };
          // Subscribe to specified endpoint
          if (endpoint) {
            this.endpoint = endpoint;
            this._subscribe();
          } else {
            this.endpoint = false;
          }
        };
        DataModel.prototype.setEndpoint = function setEndpoint(endpoint) {
          var self = this;
          self.endpoint = endpoint;
          // Subscribe to specified endpoint
          self._subscribe();
        };
        DataModel.prototype.setScope = function setScope(scope, nameObject, nameObjects, nameCount) {
          var self = this;
          self.scope = scope;
          self.itemNames = {
            object: nameObject || false,
            objects: nameObjects || false,
            count: nameCount || false
          };
        };
        DataModel.prototype.handlerCreated = function handlerCreated(message) {
          var self = this;
          $log.log('Object created', self.endpoint, message);
          // Scope is set, so we need to load collection and determine count again
          if (self.scope) {
            if (self.itemNames.objects) {
              self.load(null, true);
            }
            if (self.itemNames.count) {
              self.count(null, true);
            }
          }
        };
        DataModel.prototype.handlerUpdated = function handlerUpdated(message) {
          var self = this;
          $log.log('Object updated', self.endpoint, message);
          // Scope is set, so we need to fetch collection and object data again
          if (self.scope) {
            if (self.itemNames.object && parseInt(message.id, 10) === parseInt(self.object.id, 10)) {
              self.fetch(null, null, true);
            }
            if (self.itemNames.objects) {
              self.load(null, true);
            }
          }
        };
        DataModel.prototype.handlerDeleted = function handlerDeleted(message) {
          var self = this;
          $log.log('Object deleted', self.endpoint, message);
          // Scope is set, so we need to fetch collection and object data again
          if (self.scope) {
            if (self.itemNames.object && parseInt(message.id, 10) === parseInt(self.object.id, 10)) {
              $log.warn('How to handle this case?');
            }
            if (self.itemNames.objects) {
              self.load(null, true);
            }
            if (self.itemNames.count) {
              self.count(null, true);
            }
          }
        };
        DataModel.prototype.handlerAddedTo = function handlerAddedTo(message) {
          var self = this;
          $log.log('AddedTo', self.endpoint, message);
        };
        DataModel.prototype.handlerRemovedFrom = function handlerRemovedFrom(message) {
          var self = this;
          $log.log('RemovedFrom', self.endpoint, message);
        };
        DataModel.prototype.count = function count(parameters, fromCache) {
          var self = this;
          // Normalize parameters
          parameters = parameters || {};
          fromCache = fromCache || false;
          if (fromCache) {
            parameters = self.cache.count.parameters;
          } else {
            // Store used parameters
            self.cache.count = {
              parameters: parameters
            };
          }
          return DataService
            .count(self.endpoint, parameters)
            .then(
              function onSuccess(response) {
                if (fromCache && self.scope && self.itemNames.count) {
                  self.scope[self.itemNames.count] = response.data.count;
                }
                return response.data;
              },
              function onError(error) {
                $log.error('DataModel.count() failed.', error, self.endpoint, parameters);
              }
            );
        };
        DataModel.prototype.load = function load(parameters, fromCache) {
          var self = this;
          // Normalize parameters
          parameters = parameters || {};
          fromCache = fromCache || false;
          if (fromCache) {
            parameters = self.cache.load.parameters;
          } else {
            // Store used parameters
            self.cache.load = {
              parameters: parameters
            };
          }
          return DataService
            .collection(self.endpoint, parameters)
            .then(
              function onSuccess(response) {
                self.objects = response.data;
                if (fromCache && self.scope && self.itemNames.objects) {
                  self.scope[self.itemNames.objects] = self.objects;
                }
                return self.objects;
              },
              function onError(error) {
                $log.error('DataModel.load() failed.', error, self.endpoint, parameters);
              }
            );
        };
        DataModel.prototype.fetch = function fetch(identifier, parameters, fromCache) {
          var self = this;
          // Normalize parameters
          parameters = parameters || {};
          fromCache = fromCache || false;
          if (fromCache) {
            identifier = self.cache.fetch.identifier;
            parameters = self.cache.fetch.parameters;
          } else {
            // Store identifier and used parameters to cache
            self.cache.fetch = {
              identifier: identifier,
              parameters: parameters
            };
          }
          return DataService
            .fetch(self.endpoint, identifier, parameters)
            .then(
              function onSuccess(response) {
                self.object = response.data;
                if (fromCache && self.scope && self.itemNames.object) {
                  self.scope[self.itemNames.object] = self.object;
                }
                return self.object;
              },
              function onError(error) {
                $log.error('DataModel.fetch() failed.', error, self.endpoint, identifier, parameters);
              }
            );
        };
        DataModel.prototype.create = function create(data) {
          var self = this;
          return DataService
            .create(self.endpoint, data)
            .then(
              function onSuccess(result) {
                return result;
              },
              function onError(error) {
                $log.error('DataModel.create() failed.', error, self.endpoint, data);
              }
            );
        };
        DataModel.prototype.update = function update(identifier, data) {
          var self = this;
          return DataService
            .update(self.endpoint, identifier, data)
            .then(
              function onSuccess(result) {
                return result;
              },
              function onError(error) {
                $log.error('DataModel.update() failed.', error, self.endpoint, identifier, data);
              }
            );
        };
        DataModel.prototype.delete = function deleteObject(identifier) {
          var self = this;
          return DataService
            .delete(self.endpoint, identifier)
            .then(
              function onSuccess(result) {
                return result;
              },
              function onError(error) {
                $log.error('DataModel.delete() failed.', error, self.endpoint, identifier);
              }
            );
        };
        DataModel.prototype._subscribe = function subscribe() {
          var self = this;
          // Actual subscribe
          $sailsSocket
            .subscribe(self.endpoint, function modelEvent(message) {
              // Handle socket event
              self._handleEvent(message);
            });
        };
        DataModel.prototype._handleEvent = function handleEvent(message) {
          var self = this;
          var method = 'handler' + message.verb[0].toUpperCase() + message.verb.slice(1);

          if (_.isFunction(self[method])) {
            self[method](message);
          } else {
            $log.log('Implement handling for \'' + message.verb + '\' socket messages');
          }
        };
        return DataModel;
      }
    );
}());
