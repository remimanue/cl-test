(function() {
  'use strict';

  angular.module('frontend.core.interceptors')
    .factory('AuthInterceptor',
      function($q, $injector, $localStorage) {
        return {
          request: function requestCallback(config) {
            var token;
            // Yeah we have some user data on local storage
            if ($localStorage.credentials) {
              token = $localStorage.credentials.token;
            }
            // Yeah we have a token
            if (token) {
              if (!config.data) {
                config.data = {};
              }
              /**
               * Set token to actual data and headers. Note that we need bot ways because of socket cannot modify
               * headers anyway. These values are cleaned up in backend side policy (middleware).
               */
              config.data.token = token;
              config.headers.authorization = 'Bearer ' + token;
            }
            return config;
          },
          responseError: function responseErrorCallback(response) {
            if (response.status === 401) {
              $localStorage.$reset();
              $injector.get('$state').go('auth.login');
            }
            return $q.reject(response);
          }
        };
      }
    );
}());
