(function() {
  'use strict';

  angular.module('frontend.core.interceptors').factory('ErrorInterceptor',
      function($q, $injector) {
        return {
          response: function responseCallback(response) {
            if (response.data.error &&
              response.data.status &&
              response.data.status !== 200) {
              return $q.reject(response);
            } else {
              return response || $q.when(response);
            }
          },
          responseError: function responseErrorCallback(response) {
            var message = '';
            if (response.data && response.data.error) {
              message = response.data.error;
            } else if (response.data && response.data.message) {
              message = response.data.message;
            } else {
              if (typeof response.data === 'string') {
                message = response.data;
              } else if (response.statusText) {
                message = response.statusText;
              } else {
                message = $injector.get('HttpStatusService').getStatusCodeText(response.status);
              }
              message = message + ' <span class="text-small">(HTTP status ' + response.status + ')</span>';
            }
            if (message) {
              $injector.get('MessageService').error(message);
            }
            return $q.reject(response);
          }
        };
      }
    );
}());
