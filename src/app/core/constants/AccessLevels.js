(function() {
  'use strict';

  angular.module('frontend')
    .constant('AccessLevels', {
      anon: 0,
      user: 1,
      admin: 2
    });
}());
