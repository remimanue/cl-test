(function() {
  'use strict';

  angular.module('frontend.core.auth.services').factory('AuthService',
      function factory($http, $state, $localStorage,
        AccessLevels, BackendConfig, MessageService) {
        return {
          authorize: function authorize(accessLevel) {
            if (accessLevel === AccessLevels.user) {
              return this.isAuthenticated();
            } else if (accessLevel === AccessLevels.admin) {
              return this.isAuthenticated() && Boolean($localStorage.credentials.user.admin);
            } else {
              return accessLevel === AccessLevels.anon;
            }
          },
          isAuthenticated: function isAuthenticated() {
            return Boolean($localStorage.credentials);
          },
          login: function login(credentials) {
            return $http
              .post(BackendConfig.url + '/login', credentials, {withCredentials: true})
              .then(
                function(response) {
                  MessageService.success('You have been logged in.');
                  $localStorage.credentials = response.data;
                }
              )
            ;
          },
          logout: function logout() {
            $localStorage.$reset();
            MessageService.success('You have been logged out.');
            $state.go('auth.login');
          }
        };
      }
    );
}());
