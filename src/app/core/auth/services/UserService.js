(function() {
  'use strict';

  angular.module('frontend.core.auth.services').factory('UserService',
      function factory($localStorage) {
        return {
          user: function user() {
            return $localStorage.credentials ? $localStorage.credentials.user : {};
          }
        };
      }
    );
}());
