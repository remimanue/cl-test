(function() {
	'use strict';

	// Define frontend.auth.login angular module
	angular.module('frontend.core.auth.login', []);

	// Module state
	angular.module('frontend.core.auth.login')
		.constant('AuthStates', {
			url: '/login',
			data: { access: 0 },
			views: {
				'content@': {
					templateUrl: '/frontend/core/auth/login/login.html',
					controller: 'LoginController'
				}
			}
		}
	);

	// Module configuration
	angular.module('frontend.core.auth.login').config(
		function config($stateProvider, AuthStates) {
			$stateProvider.state('auth.login', AuthStates);
		}
	);
}());
