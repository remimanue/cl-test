(function() {
  'use strict';

  angular.module('frontend.core.auth.login').controller('LoginController',
	 	function controller($scope, $state, AuthService, FocusOnService) {
        	// Already authenticated so redirect back to books list
        	if (AuthService.isAuthenticated()) {
          	$state.go('examples.books');
        	}
        	// Scope function to perform actual login request to server
        	$scope.login = function login() {
          	AuthService.login($scope.credentials).then(
					function successCallback() {
						$state.go('examples.books');
					},
					function errorCallback() {
						_reset();
					}
				);
			};
			function _reset() {
				FocusOnService.focus('username');
				// Initialize credentials
				$scope.credentials = {
					identifier: '',
					password: ''
				};
			}
			_reset();
		}
	);
}());
