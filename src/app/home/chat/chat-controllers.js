(function() {
  'use strict';

  angular.module('frontend.home.chat').controller('ChatController',
      function controller($scope, $timeout, $localStorage,
        moment, MessageService, MessageModel, _messages) {

        $scope.messages = _messages;
        $scope.nick = ($localStorage.chat && $localStorage.chat.nick) ? $localStorage.chat.nick : '';
        $scope.message = {
          nick: $scope.nick,
          message: ''
        };
        if ($scope.nick && $scope.nick.trim()) {
          _scrollBottom();
        }
        $scope.$watch('messages', function watcher(valueNew) {
          if (valueNew) {
            _scrollBottom();
          }
        }, true);
        $scope.enterToChat = function enterToChat() {
          if ($scope.nick && $scope.nick.trim() !== '') {
            $scope.message.nick = $scope.nick;
            $localStorage.chat = {
              nick: $scope.nick,
              time: moment().format()
            };
            _scrollBottom();
          } else {
            MessageService.error('Please provide some nick.');
          }
        };
        $scope.leaveChat = function leaveChat() {
          $scope.message.nick = '';
          $scope.nick = '';
          $scope.messages = [];
          $localStorage.chat = {};
        };
        $scope.postMessage = function postMessage() {
          if ($scope.message.message.trim() !== '') {
            MessageModel.create($scope.message).then(
                function success() {
                  $scope.message.message = '';
                  _scrollBottom();
                }
           );
          } else {
            MessageService.error('Please enter some text to chat.');
          }
        };
        function _scrollBottom() {
          $timeout(function timeout() {
            document.getElementById('messages').scrollTop = $scope.messages.length * 50;
          });
        }
      }
   );
}());
