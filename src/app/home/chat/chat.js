(function() {
	'use strict';

	angular.module('frontend.home.chat', []);

	angular.module('frontend.home.chat').config(
		function config($stateProvider) {
			$stateProvider.state('home.chat', {
				url: '/home/chat',
				views: {
					'content@': {
						templateUrl: '/frontend/home/chat/chat.html',
						controller: 'ChatController',
						resolve: {
							_messages: function resolve($localStorage, moment, MessageModel) {
								var parameters = {
									where: {
										createdAt: {
											'>': ($localStorage.chat && $localStorage.chat.time) ?
													$localStorage.chat.time : moment().format()
										}
									}
								};
								return MessageModel.load(parameters);
							}
						}
					}
				}
			});
		}
	);
}());
