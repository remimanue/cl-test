(function() {
  'use strict';

  angular.module('frontend.home.chat').factory('MessageModel',
      function factory(DataModel) {
        var model = new DataModel('message');
        // Custom handler for created objects
        model.handlerCreated = function handlerCreated(message){
          this.objects.push(message.data);
        };
        return model;
      }
   );
}());
