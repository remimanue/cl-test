(function() {
  'use strict';

  angular.module('frontend.home.messages').controller('MessagesController',
      function($scope, $http, $sailsSocket,
        MessageService, BackendConfig) {

        $scope.title = '';
        $scope.message = '';
        $scope.type = 'info';
        $scope.messageTypes = [
          'info', 'success', 'warning', 'error'
        ];
        var urls = [
          BackendConfig.url + '/Basdfasdf',
          BackendConfig.url + '/Book/123123123'
        ];
        $scope.showMessage = function showMessage() {
          MessageService[$scope.type]($scope.message, $scope.title);
        };
        $scope.makeInvalidHttpRequest = function makeInvalidHttpRequest(type) {
          $http.get(urls[type]);
        };
        $scope.makeInvalidSailsSocketRequest = function makeInvalidSailsSocketRequest(type) {
          $sailsSocket.get(urls[type]);
        };
      }
	);
}());
