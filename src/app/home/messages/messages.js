(function() {
  'use strict';

  angular.module('frontend.home.messages', []);
  angular.module('frontend.home.messages').config(
      function config($stateProvider) {
        $stateProvider.state('home.messages', {
            url: '/home/messages',
            views: {
              'content@': {
                templateUrl: '/frontend/home/messages/messages.html',
                controller: 'MessagesController'
              }
            }
      	});
      }
   );
}());
