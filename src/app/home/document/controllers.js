(function() {
	'use strict';

	angular.module('frontend.home.document')
		.controller('DocumentAddController',
			function controller($scope, $state,
					MessageService, DocumentModel, _utilisateurs) {
				$scope.utilisateurs = _utilisateurs;
        		$scope.document = {
					title: '',
					description: '',
					utilisateur: '',
					releaseDate: new Date()
				};

				 $scope.addDocument = function addDocument() {
					 DocumentModel.create(angular.copy($scope.document)).then(
						 function onSuccess(result) {
							MessageService.success('New document added successfully');
							$state.go('home.document', {id: result.data.id});
						}
					);
				};
			}
		);

		angular.module('frontend.home.document')
			.controller('DocumentController',
				function controller($scope, $state,
						UserService, MessageService,
						DocumentModel, UtilisateurModel, _document) {

					DocumentModel.setScope($scope, 'document');
					$scope.user = UserService.user();
					$scope.document = _document;
					$scope.utilisateurs = [];
					$scope.selectUtilisateur = _document.utilisateur ? _document.utilisateur.id : null;
					$scope.confirmButtonsDelete = {
						ok: {
							label: 'Delete',
							className: 'btn-danger',
							callback: function callback() {
								$scope.deleteDocument();
							}
						},
						cancel: {
							label: 'Cancel',
							className: 'btn-default pull-left'
						}
					};

					$scope.saveDocument = function saveDocument() {
						var data = angular.copy($scope.document);
						data.utilisateur = $scope.selectUtilisateur;
						DocumentModel.update(data.id, data).then(
							function onSuccess() {
								MessageService.success('Document "' + $scope.document.title + '" updated successfully');
							}
						);
					};

					$scope.deleteDocument = function deleteDocument() {
						DocumentModel.delete($scope.document.id).then(
							function onSuccess() {
								MessageService.success('Document "' + $scope.document.title + '" deleted successfully');
								$state.go('home.documents');
							}
						);
 					};

					$scope.loadUtilisateurs = function loadUtilisateurs() {
						if ($scope.utilisateurs.length) {
							return null;
						} else {
							return UtilisateurModel.load().then(
								function onSuccess(data) {
									$scope.utilisateurs = data;
								}
							);
						}
					};
				}
			);

		angular.module('frontend.home.document')
			.controller('DocumentListController',
				function controller($scope, $q, $timeout, _,
						ListConfig, SocketHelperService, UserService,
						DocumentModel, UtilisateurModel, _items, _count, _utilisateurs) {

					DocumentModel.setScope($scope, false, 'items', 'itemCount');
					UtilisateurModel.setScope($scope, false, 'utilisateurs');
					$scope = angular.extend($scope, angular.copy(ListConfig.getConfig()));
					$scope.items = _items;
					$scope.itemCount = _count.count;
					$scope.utilisateurs = _utilisateurs;
					$scope.user = UserService.user();
					$scope.titleItems = ListConfig.getTitleItems(DocumentModel.endpoint);
					$scope.sort = {
						column: 'releaseDate',
						direction: false
					};
					$scope.filters = {
						searchWord: '',
						columns: $scope.titleItems
					};
					$scope.changeSort = function changeSort(item) {
						var sort = $scope.sort;
						if (sort.column === item.column) {
							sort.direction = !sort.direction;
						} else {
							sort.column = item.column;
							sort.direction = true;
						}
						_triggerFetchData();
					};

					$scope.getUtilisateur = function getUtilisateur(utilisateurId, property, defaultValue) {
						defaultValue = defaultValue || 'Unknown';
						property = property || true;
						// Find utilisateur
						var utilisateur = _.find($scope.utilisateurs, function iterator(utilisateur) {
							return parseInt(utilisateur.id, 10) === parseInt(utilisateurId.toString(), 10);
						});
						return utilisateur ? (property === true ? utilisateur : utilisateur[property]) : defaultValue;
					};

					$scope.$watch('currentPage', function watcher(valueNew, valueOld) {
						if (valueNew !== valueOld) { _fetchData(); }
					});

					$scope.$watch('itemsPerPage', function watcher(valueNew, valueOld) {
						if (valueNew !== valueOld) { _triggerFetchData(); }
					});

					var searchWordTimer;
					$scope.$watch('filters', function watcher(valueNew, valueOld) {
						if (valueNew !== valueOld) {
							if (searchWordTimer) {
								$timeout.cancel(searchWordTimer);
							}
							searchWordTimer = $timeout(_triggerFetchData, 400);
						}
					}, true);

					function _triggerFetchData() {
						if ($scope.currentPage === 1) {
							_fetchData();
						} else {
							$scope.currentPage = 1;
						}
					}

					function _fetchData() {
						$scope.loading = true;
						var commonParameters = {
							where: SocketHelperService.getWhere($scope.filters)
						};
						var parameters = {
							limit: $scope.itemsPerPage,
							skip: ($scope.currentPage - 1) * $scope.itemsPerPage,
							sort: $scope.sort.column + ' ' + ($scope.sort.direction ? 'ASC' : 'DESC')
						};
						var count = DocumentModel.count(commonParameters).then(
							function onSuccess(response) {
								$scope.itemCount = response.count;
							}
						);
						var load = DocumentModel.load(_.merge({}, commonParameters, parameters)).then(
							function onSuccess(response) {
								$scope.items = response;
							}
						);
						$q.all([count, load]).finally(
							function onFinally() {
								$scope.loaded = true;
								$scope.loading = false;
							}
						);
					}
				}
		);
}());
