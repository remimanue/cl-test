(function() {
  'use strict';

  angular.module('frontend.home.document', []);
  angular.module('frontend.home.document').config(
      function config($stateProvider) {
        $stateProvider.state('home.documents', {
            url: '/home/documents',
            views: {
              'content@': {
                templateUrl: '/frontend/home/document/list.html',
                controller: 'DocumentListController',
                resolve: {
                  _items: function resolve(ListConfig, DocumentModel) {
                      var config = ListConfig.getConfig();
                      var parameters = {
                        limit: config.itemsPerPage,
                        sort: 'releaseDate DESC'
                      };
                      return DocumentModel.load(parameters);
                  },
                  _count: function resolve(DocumentModel) {
                      return DocumentModel.count();
                  },
                  _utilisateurs: function resolve(UtilisateurModel) {
                      return UtilisateurModel.load();
                  }
                }
              }
            }
          }).state('home.document', {
            url: '/home/document/:id',
            views: {
              'content@': {
                templateUrl: '/frontend/home/document/document.html',
                controller: 'DocumentController',
                resolve: {
                  _document: function resolve($stateParams, DocumentModel) {
                      return DocumentModel.fetch($stateParams.id, {populate: 'utilisateur'});
                 	}
                }
              }
            }
          }).state('home.document.add', {
            url: '/home/document/add',
            data: { access: 2 },
            views: {
              'content@': {
                templateUrl: '/frontend/home/document/add.html',
                controller: 'DocumentAddController',
                resolve: {
                  _utilisateurs: function resolve(UtilisateurModel) {
                      return UtilisateurModel.load();
                 	}
                }
              }
            }
      	});
      }
   );
}());
