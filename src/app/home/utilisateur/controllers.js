(function() {
  'use strict';

  angular.module('frontend.home.utilisateur')
    .controller('UtilisateurAddController',
      function controller($scope, $state,
        MessageService, UtilisateurModel) {

        $scope.utilisateur = {
          name: '',
          description: ''
        };
        $scope.addUtilisateur = function addUtilisateur() {
          UtilisateurModel
            .create(angular.copy($scope.utilisateur))
            .then(
              function onSuccess(result) {
                MessageService.success('New utilisateur added successfully');
                $state.go('home.utilisateur', {id: result.data.id});
              }
            );
        };
      }
    );

  angular.module('frontend.home.utilisateur').controller('UtilisateurController',
	       function controller($scope, $state,
        UserService, MessageService,
        UtilisateurModel, DocumentModel,
        _utilisateur, _documents, _documentsCount) {

        UtilisateurModel.setScope($scope, 'utilisateur');
        DocumentModel.setScope($scope, false, 'documents', 'documentsCount');
        $scope.user = UserService.user();
        $scope.utilisateur = _utilisateur;
        $scope.documents = _documents;
        $scope.documentsCount = _documentsCount.count;
        $scope.confirmButtonsDelete = {
          ok: {
            label: 'Delete',
            className: 'btn-danger',
            callback: function callback() {
              $scope.deleteUtilisateur();
            }
          },
          cancel: {
            label: 'Cancel',
            className: 'btn-default pull-left'
          }
        };
        $scope.saveUtilisateur = function saveUtilisateur() {
          var data = angular.copy($scope.utilisateur);
          UtilisateurModel
            .update(data.id, data)
            .then(
              function onSuccess() {
                MessageService.success('Utilisateur "' + $scope.utilisateur.name + '" updated successfully');
              }
            );
        };
        $scope.deleteUtilisateur = function deleteUtilisateur() {
          UtilisateurModel
            .delete($scope.utilisateur.id)
            .then(
              function onSuccess() {
                MessageService.success('Utilisateur "' + $scope.utilisateur.name + '" deleted successfully');
                $state.go('home.utilisateurs');
              }
            );
        };
      }
   );

  angular.module('frontend.home.utilisateur').controller('UtilisateurListController',
      function controller($scope, $q, $timeout, _,
        ListConfig, SocketHelperService, UserService,
		   UtilisateurModel, _items, _count) {

        UtilisateurModel.setScope($scope, false, 'items', 'itemCount');
        $scope = angular.extend($scope, angular.copy(ListConfig.getConfig()));
        $scope.items = _items;
        $scope.itemCount = _count.count;
        $scope.user = UserService.user();
        $scope.titleItems = ListConfig.getTitleItems(UtilisateurModel.endpoint);
        $scope.sort = {
          column: 'name',
          direction: true
        };
        $scope.filters = {
          searchWord: '',
          columns: $scope.titleItems
        };
        $scope.changeSort = function changeSort(item) {
          var sort = $scope.sort;
          if (sort.column === item.column) {
            sort.direction = !sort.direction;
          } else {
            sort.column = item.column;
            sort.direction = true;
          }
          _triggerFetchData();
        };

        $scope.$watch('currentPage', function watcher(valueNew, valueOld) {
          if (valueNew !== valueOld) {
            _fetchData();
          }
        });

        $scope.$watch('itemsPerPage', function watcher(valueNew, valueOld) {
          if (valueNew !== valueOld) {
            _triggerFetchData();
          }
        });

        var searchWordTimer;
        $scope.$watch('filters', function watcher(valueNew, valueOld) {
          if (valueNew !== valueOld) {
            if (searchWordTimer) {
              $timeout.cancel(searchWordTimer);
            }
            searchWordTimer = $timeout(_triggerFetchData, 400);
          }
        }, true);

        function _triggerFetchData() {
          if ($scope.currentPage === 1) {
            _fetchData();
          } else {
            $scope.currentPage = 1;
          }
        }
        function _fetchData() {
          $scope.loading = true;
          var commonParameters = {
            where: SocketHelperService.getWhere($scope.filters)
          };
          var parameters = {
            populate: 'documents',
            limit: $scope.itemsPerPage,
            skip: ($scope.currentPage - 1) * $scope.itemsPerPage,
            sort: $scope.sort.column + ' ' + ($scope.sort.direction ? 'ASC' : 'DESC')
          };
          var count = UtilisateurModel.count(commonParameters)
            .then(
              function onSuccess(response) {
                $scope.itemCount = response.count;
              }
            );
          var load = UtilisateurModel
            .load(_.merge({}, commonParameters, parameters))
            .then(
              function onSuccess(response) {
                $scope.items = response;
              }
            );
          $q.all([count, load]).finally(
              function onFinally() {
                $scope.loaded = true;
                $scope.loading = false;
              }
            );
        }
      }
    );
}());
