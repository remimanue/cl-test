(function() {
  'use strict';

  angular.module('frontend.home.utilisateur', []);
  angular.module('frontend.home.utilisateur').config(
      function config($stateProvider) {
        $stateProvider.state('home.utilisateurs', {
            url: '/home/utilisateurs',
            views: {
              'content@': {
                templateUrl: '/frontend/home/utilisateur/list.html',
                controller: 'UtilisateurListController',
                resolve: {
                  _items: function resolve(ListConfig, UtilisateurModel) {
                      var config = ListConfig.getConfig();
                      var parameters = {
                        populate: 'documents',
                        limit: config.itemsPerPage,
                        sort: 'name ASC'
                      };
                      return UtilisateurModel.load(parameters);
                  },
                  _count: function resolve(UtilisateurModel) {
                      return UtilisateurModel.count();
                  }
                }
              }
            }
          }).state('home.utilisateur', {
            url: '/home/utilisateur/:id',
            views: {
              'content@': {
                templateUrl: '/frontend/home/utilisateur/utilisateur.html',
                controller: 'UtilisateurController',
                resolve: {
                  _utilisateur: function resolve($stateParams, UtilisateurModel) {
                      return UtilisateurModel.fetch($stateParams.id);
                 	},
                  _documents: function resolve($stateParams, DocumentModel) {
                      return DocumentModel.load({utilisateur: $stateParams.id});
                 	},
                  _documentsCount: function resolve($stateParams, DocumentModel) {
                      return DocumentModel.count({utilisateur: $stateParams.id});
                 	}
                }
              }
            }
          }).state('home.utilisateur.add', {
            url: '/home/utilisateur/add',
            data: { access: 2 },
            views: {
              'content@': {
                templateUrl: '/frontend/home/utilisateur/add.html',
                controller: 'UtilisateurAddController'
              }
            }
          });
      }
   );
}());
