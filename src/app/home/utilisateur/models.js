(function() {
  'use strict';

  angular.module('frontend.home.utilisateur').service('UtilisateurModel',
      function(DataModel) {
        return new DataModel('utilisateur');
      }
   );
}());
