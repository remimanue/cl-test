(function() {
  'use strict';

  angular.module('frontend.home', [
    'frontend.home.utilisateur',
    'frontend.home.document',
    'frontend.home.chat',
    'frontend.home.messages'
  ]);
  angular.module('frontend.home').config(function($stateProvider) {
		  $stateProvider.state('home', {
            parent: 'frontend',
            data: { access: 1 },
            views: {
              'content@': {
                controller: function($state) {
                    $state.go('home.documents');
               }
              },
              'pageNavigation@': {
                templateUrl: '/frontend/core/layout/partials/navigation.html',
                controller: 'NavigationController',
                resolve: {
                  _items: function resolve(ContentNavigationItems) {
                      return ContentNavigationItems.getItems('home');
                    }
                }
              }
            }
          });
      }
    );
}());
