/**
 * Toaster
 */
(function() {
	'use strict';

	var constants = {
		allowHtml: true,
		closeButton: true,
		extendedTimeOut: 3000
	};

	function config(toastrConfig, ToasterConfigDefault) {
		// Extend default toastr configuration with application specified configuration
		angular.extend(toastrConfig, ToasterConfigDefault);
	}

	// function factory(toastr, toastrConfig, ToaterConfigDefault) {
	// 	return function(options) {
	// 		angular.extend(ToaterConfigDefault, options);
	// 		angular.extend(toastrConfig, ToaterConfigDefault);
	// 		return toastr;
	// 	};
	// }
	// Create module and specify dependencies for that
	angular.module('app.services.toaster', ['toastr']);
	angular.module('app.services.toaster').constant('ToasterConfigDefault', constants);
	angular.module('app.services.toaster').config(config);
	// angular.module('app.services.toater').factory('Toaster', factory);
}());
