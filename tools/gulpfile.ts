import {Gulpclass, SequenceTask, Task} from 'gulpclass';
import * as fs from 'fs';
import * as path from 'path';
import * as logger from './logger';
import * as Config from './config';
// logger.debug('CFG', Config);

const Tasks = {};
const Register = {};
const files = (dir: string) => {
	let mapping = (name: string) => name.replace('.ts', '');
	return fs.readdirSync(dir).map(mapping);
};
const load = (dir:string, name: string) => {
	let file = path.join(dir, name);
	// logger.debug('Loading ' + file + ' ...', null);
	try { return require(file); }
	catch (e) {
		logger.debug('Loading ' + file + ' Error', e);
		return false;
	}
};
const config = (dir: string) => {
	let p = path.join(__dirname, dir, 'config');
	// logger.debug('Loading ' + p + ' ...', null);
	let mapping = (name: string) =>
	 	Tasks[name] = load(p, name);
	files(p).map(mapping);
	return Tasks;
};
const register = (dir: string) => {
	let p = path.join(__dirname, dir, 'register');
	// logger.debug('Loading ' + p + ' ...', null);
	let mapping = (name: string) =>
		Register[name] = load(p, name);
	files(p).map(mapping);
	return Register;
};

@Gulpclass(Config.getGulp())
export class Gulpfile {
	config = config;
	register = register;
	/** Clean */
	@SequenceTask()
	clean() { return [
		'clean:dev',
		'clean:prod',
	]; }

	/** Build */
	@SequenceTask()
	build() { return [
		'clean:dev',
		'build:dev',
		'link:dev'
	]; }
	/** Dist */
	@SequenceTask()
	dist() { return [
		'clean:prod',
		'build:prod',
		'link:prod'
	]; }

	/** Default task TODO for sails-hook-gulp */
	@SequenceTask()
	default() { return ['watch:dev']; }
	/** Prod task TODO for sails-hook-gulp */
	@SequenceTask()
	prod() { return ['watch:prod']; }

	/** Start in Development mode */
	@SequenceTask()
	serve() { return [
		'clean:dev',
		'build:dev',
		'link:dev',
		'watch:dev',
		'serve:dev'
	]; }
	/** Start in Production mode */
	@SequenceTask()
	start() { return [
		'clean:prod',
		'build:prod',
		'link:prod',
		// 'watch:prod',
		'serve:prod'
	]; }
	/** Test Karma run once */
	@SequenceTask()
	test() { return [
		'build:test', // build:test
		'link:test', // link:test
		'run:test', // karma:run
	]; }
	/** TODO Serve Test for debugging */
	@SequenceTask()
	debug() { return [
		'build:test', // build:test
		'link:test', // link:test
		// 'watch:test', // 'karma:watch'
		// 'serve:test', // karma:start
	]; }
}
