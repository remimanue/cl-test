import * as fs from 'fs';

export const ROOT = process.cwd();
export const GULP = require('gulp');
export const getGulp = () => GULP;
export const PACK = require('../package.json');

function getBower() {
	let bower;
	try { bower = require('../bower.json'); }
	catch (e) { bower = false; }
	return bower;
}

export const BOWER = getBower();
export const NAME = (BOWER || PACK).name;

function loadSettings() {
	let settings;
	let file = (name: string) => fs.readFileSync(name, 'utf8');
	// Try to read configuration file, fallback to default file
	try { settings = file('./src/config/config.json'); }
	catch (error) { settings = file('./src/config/config_example.json'); }
	return JSON.parse(settings);
}
const SETTINGS = loadSettings();
export const SETTING = (env: string) =>
	SETTINGS[env === 'prod' ? 'production' : 'development'];
