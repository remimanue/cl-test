import {Gulpclass, Task} from 'gulpclass';
import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class GulpClean extends Pipes.Gulp {
	app = ['./app', './assets']; // this.dev.path
	dist = './dist'; //this.prod.path
	vendor = {
		dev: './assets/vendors',
		prod: '/dist/vendors.*'
	};
	clean = () => this.pipes()
		.pipe(this.plugins('clean'))();

	@Task('clean:vendor:dev')
	cleanDevVendor() {
		return this.src(this.vendor.dev)
			.pipe(this.clean());
	}
	@Task('clean:vendor:prod')
	cleanProdVendor() {
		return this.src(this.vendor.prod)
			.pipe(this.clean());
	}
	@Task('clean:app:dev')
	cleanDevStyles() {
		return this.src(this.app)
			.pipe(this.clean());
	}
	@Task('clean:app:prod')
	cleanProdStyles() {
		return this.src(this.dist)
			.pipe(this.clean());
	}
}
