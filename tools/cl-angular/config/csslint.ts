import {Gulpclass, Task} from 'gulpclass';
import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class GulpCSSLint extends Pipes.Gulp {
	options = './.csslintrc';
	format = 'stylish';
	dev = {
		src: './app/**/*.css',
		opts: this.options,
		format: this.format,
	};
	prod = {
		src: './dist/styles/**/*.css',
		opts: this.options,
		format: this.format,
	};
	csslint = (cfg) => this.pipes()
    	.pipe(this.plugins('csslint'), cfg.opts)
		.pipe(this.plugins('csslint').formatter, cfg.format)();

	@Task('css:lint:dev')
	cssLintDev() {
		return this.cached(this.dev.src, 'csslint')
			.pipe(this.csslint(this.dev))
			.pipe(this.reload());
	}
	@Task('css:lint:prod')
	cssLintProd() {
		return this.src(this.prod.src)
			.pipe(this.csslint(this.prod));
	 }
}
