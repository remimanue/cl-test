import {Gulpclass, Task} from 'gulpclass';
import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class GulpSASS extends Pipes.Gulp {
	sass = this.plugins('sass');
	// options = {};

	sources = [
		'./src/app/**/*.s+(a|c)ss',
		'!./src/app/styles/**/*.s+(a|c)ss'
	];
	dev = {
		src: this.sources,
		dest: './app'
	};
	prod = {
		src: this.sources,
		dest: './dist/styles'
	};

	pipe = (cfg) => this.pipes()
		.pipe(this.sass)
		.pipe(this.dest, cfg.dest)();

	@Task('sass:dev')
	sassDev() {
		return this.cached(this.sources, 'built-css')
			.pipe(this.pipe(this.dev))
	}
	@Task('sass:force')
	sassForce() {
		return this.src(this.sources)
			.pipe(this.pipe(this.dev))
	}
	@Task('sass:prod')
	sassProd() {
		return this.src(this.sources)
			.pipe(this.pipe(this.prod));
	}
}
