import {Gulpclass, Task} from 'gulpclass';
import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class GulpHTMLMin extends Pipes.Gulp {
	options = {
	  removeComments: true,
	  collapseWhitespace: true,
	  removeEmptyAttributes: false,
	  collapseBooleanAttributes: true,
	  removeRedundantAttributes: true
	};
	partials = {
		src: ['./src/app/**/*.html', '!./src/app/index.html'],
		opts: this.options,
		dest: './dist/templates'
	};
	htmlmin = (cfg) => this.pipes()
    	.pipe(this.plugins('htmlmin'), cfg.opts)
		.pipe(this.dest, cfg.dest)();

	@Task('html:min:partials')
	htmlMin() {
		return this.src(this.partials.src)
			.pipe(this.htmlmin(this.partials));
	}
}
