import {Gulpclass, Task} from 'gulpclass';
import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class GulpInject extends Pipes.Gulp {
	embedlr = this.plugins('embedlr');
	replace = this.plugins('replaceTask');
	bowerFiles = this.plugins('bowerFiles');

	styles = './app/**/*.css';
	scripts = [
		'./app/**/*.js',
		'!./.tmp/**/*test.js',
		'!./.tmp/partials/**/*.js'
	];

	replacement = (env) => this.settings(env).backend.url;
	patterns = (env) => [{
		match: 'backendUrl',
		replacement: this.replacement(env)
	}];

	dev = {
		vendor: {
			files: () => this.src(this.bowerFiles(), { read: false }),
			opts: { name: 'vendor', ignorePath: 'bower_components' }
		},
		scripts: {
			files: () => this.src(this.scripts).pipe(this.sort('angularFilesort')),
			opts: { ignorePath: 'app' }
		},
		styles: {
			files: () => this.src(this.styles, { read: false }),
			opts: { ignorePath: 'app' }
		}
	};
	prod = {
		vendor: {
			files: () => this.src('./dist/vendors.min.{js,css}', { read: false }),
			opts: { name: 'vendor', ignorePath: 'dist' }
		},
		app: {
			files: () => this.src('./dist/' + this.name + '*.min.{js,css}', { read: false }),
			opts: { ignorePath: 'dist' }
		},
	}
	inject = (cfg) => this.pipes()
		.pipe(this.plugins('inject'), cfg.files(), cfg.opts)
		.pipe(this.debug, 'Injected')();

	@Task('inject:dev')
	injectDev() {
	   return this.src('./src/app/index.html')
			.pipe(this.inject(this.dev.vendor))
			.pipe(this.inject(this.dev.scripts))
			.pipe(this.inject(this.dev.styles))
	  		.pipe(this.embedlr())
			.pipe(this.replace({ patterns: this.patterns('dev') }))
  			.pipe(this.dest('./app'))
			.pipe(this.reload());
 	}
	@Task('inject:prod')
	injectProd() {
		return this.src('./src/app/index.html')
			.pipe(this.inject(this.prod.vendor))
			.pipe(this.inject(this.prod.app))
			.pipe(this.replace({ patterns: this.patterns('prod') }))
			//.pipe(g.htmlmin(htmlminOpts))
			.pipe(this.dest('./dist/'));
 	}
}
