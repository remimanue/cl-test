import {Gulpclass, Task} from 'gulpclass';
import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class GulpHTML2JS extends Pipes.Gulp {
	options = {
		moduleName: this.name + '-templates',
      prefix: '/' + this.name + '/',
      stripPrefix: '/src/app'
	};
	dev = {
		src: ['./src/app/**/*.html', '!./src/app/index.html'],
		opts: this.options,
		dest: './app'
	};
	prod = {
		src: './dist/templates/**/*.html',
		opts: this.options,
		dest: './dist/partials'
	};
	ngHtml2js = (cfg) => this.pipes()
    	.pipe(this.plugins('ngHtml2js'), cfg.opts)();

	@Task('ng:html:2js:dev')
	html2jsDev() {
		return this.src(this.dev.src)
			.pipe(this.ngHtml2js(this.dev))
			.pipe(this.rename({ extname: '.partials.js' }))
			.pipe(this.dest(this.dev.dest))
			// .pipe(this.reload())
	}
	@Task('ng:html:2js:prod')
	html2jsPrud() {
		return this.src(this.prod.src)
			.pipe(this.ngHtml2js(this.prod))
			// .pipe(this.rename({ extname: '.js' }))
			.pipe(this.dest(this.prod.dest))
 	}
}
