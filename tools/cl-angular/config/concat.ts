import {Gulpclass, Task} from 'gulpclass';
import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class GulpConcat extends Pipes.Gulp {
	styles = {
		src: './dist/styles/**/*.min.css',
		opts: this.name + '.css',
		dest: './dist'
	};
	partials = {
		src: './dist/partials/**/*.min.js',
		opts: this.name + '-templates.js',
		dest: './dist'
	};
	scripts = {
		src: './dist/scripts/**/*.min.js',
		opts: this.name + '.js',
		dest: './dist'
	};
	concat = (cfg) => this.pipes()
		.pipe(this.plugins('concat'), cfg.opts)
		.pipe(this.dest, cfg.dest)();

	@Task('concat:partials')
	concatPartials() {
		return this.src(this.partials.src)
			.pipe(this.concat(this.partials));
 	}
	@Task('concat:styles')
	concatStyles() {
		return this.src(this.styles.src)
			.pipe(this.concat(this.styles));
	}
	@Task('concat:scripts')
	concatScripts() {
		return this.src(this.scripts.src)
			.pipe(this.sort('angularFilesort'))
			.pipe(this.concat(this.scripts));
 	}
}
