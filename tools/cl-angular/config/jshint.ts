import {Gulpclass, Task} from 'gulpclass';
import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class GulpJSHint extends Pipes.Gulp {
	options = './.jshintrc';
	format = 'jshint-stylish';

	dev = {
		partials: {
			src: './app/**/*.partials.js',
			opts: { strict: 'implied' },
			format: this.format
		},
		scripts: {
			src: ['./app/**/*.js', '!./app/**/*.partials.js'],
			opts: this.options,
			format: this.format
		}
	};
	prod = {
		partials: {
			src: './dist/partials/**/*.js',
			opts: { strict: 'implied' },
			format: this.format
		},
		scripts: {
			src: './dist/scripts/**/*.js',
			opts: this.options,
			format: this.format
		}
	};

	jshint = (cfg) => this.pipes()
		.pipe(this.plugins('jshint'), cfg.opts)
		.pipe(this.plugins('jshint').reporter, cfg.format)();
		// .pipe(this.plugins('jshint').reporter, 'fail')

	@Task('js:hint:partials:dev')
	jsHintPartialsDev() {
		return this.cached(this.dev.partials.src, 'jshint-partials')
			.pipe(this.jshint(this.dev.partials))
			.pipe(this.reload());
	}
	@Task('js:hint:partials:prod')
	jsHintPartialsProd() {
		return this.src(this.prod.partials.src)
			.pipe(this.jshint(this.prod.partials));
	}
	@Task('js:hint:scripts:dev')
	jsHintScriptsDev() {
		return this.cached(this.dev.scripts.src, 'jshint-scripts')
			.pipe(this.jshint(this.dev.scripts))
			.pipe(this.reload());
	}
	@Task('js:hint:scripts:prod')
	jsHintScriptsProd() {
		return this.src(this.prod.scripts.src)
			.pipe(this.jshint(this.prod.scripts));
	}
}
