import {Gulpclass, Task} from 'gulpclass';
import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class GulpBower extends Pipes.Gulp {
	bowerFiles = this.plugins('bowerFiles');
	concat = this.plugins('concat');
	cssnano = this.plugins('cssnano');
	rename = this.plugins('rename');
	uglify = this.plugins('uglify');

	fonts = this.bowerFiles(/fonts/);
	styles = this.bowerFiles(/.css/);
	scripts = this.bowerFiles(/.js/);

	dev = (src) => this.src(src, { base: './bower_components' })
		.pipe(this.dest('./assets/vendors'));

	@Task('bower:fonts:dev')
	bowerDevFonts() { return this.dev(this.fonts); }
	@Task('bower:scripts:dev')
	bowerDevScripts() { return this.dev(this.scripts); }
	@Task('bower:styles:dev')
	bowerDevStyles() { return this.dev(this.styles); }
	@Task('bower:fonts:prod')
	bowerProdFonts() {
		return this.src(this.fonts)
			.pipe(this.dest('./dist/fonts'));
	}
	@Task('bower:styles:prod')
	bowerProdStyles() {
		return this.src(this.styles)
			.pipe(this.cssnano())
			.pipe(this.rename({ extname: '.min.css' }))
			.pipe(this.concat('vendors.css'))
			.pipe(this.dest('./dist/styles'));
	}
	@Task('bower:scripts:prod')
	bowerProdScripts() {
		return this.src(this.scripts)
			.pipe(this.uglify())
			.pipe(this.rename({ extname: '.min.js' }))
			.pipe(this.concat('vendors.js'))
			.pipe(this.dest('./dist/scripts'));
	}
}
