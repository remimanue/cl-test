import {Gulpclass, Task} from 'gulpclass';
import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class GulpSASSLint extends Pipes.Gulp {
	sasslint = this.plugins('sassLint');
	assets = './src/styles/**/*.s+(a|c)ss';
	sources = './src/app/**/*.s+(a|c)ss';
	options = { configFile: '.sass-lint.yml' };
	// format = 'stylish';
	pipe = () => this.pipes()
    	.pipe(this.sasslint, this.options)
		.pipe(this.sasslint.format)();

	@Task('sass:lint:assets')
	sassLintAssets() {
		return this.cached(this.assets, 'assets:sass')
			.pipe(this.pipe());
	}
	@Task('sass:lint:prod')
	sassLint() {
		return this.src(this.sources)
			.pipe(this.pipe());
	}
	@Task('sass:lint:dev')
	sassLintCached() {
		return this.cached(this.sources, 'sass:linted')
			.pipe(this.pipe());
	}
}
