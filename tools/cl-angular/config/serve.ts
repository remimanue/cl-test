import {Gulpclass, Task} from 'gulpclass';
import * as Pipes from '../pipeline';

const historyApiFallback = require('connect-history-api-fallback');

@Gulpclass(Pipes.getGulp())
export class GulpServe extends Pipes.Gulp {
	/** Static file server */
	app = {
		hostname: 'localhost',
		port: this.settings('dev').frontend.port,
		root: ['./app', './assets', './bower_components'],
		middleware: historyApiFallback({})
	};
	/** Production file server, note remember to run 'gulp dist' first! */
	dist = {
		hostname: 'localhost',
		port: this.settings('prod').frontend.port,
		root: ['./dist'],
		middleware: historyApiFallback({})
	};

	serve = (opts) => this.plugins('serve')(opts)();

	@Task('serve:app')
	serveApp(done: Function) {
		this.serve(this.app);
	 	done();
	}
	@Task('serve:dist')
	serveProd(done: Function) {
		this.serve(this.dist);
		done();
	}
}
