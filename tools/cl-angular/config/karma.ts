import {Gulpclass, Task} from 'gulpclass';
import * as fs from 'fs';
import * as path from 'path';
import * as Pipes from '../pipeline';

// const Server = require('karma').Server;
let Queue = require('streamqueue');

@Gulpclass(Pipes.getGulp())
export class Gulpfile extends Pipes.Gulp {
	inject = this.plugins('inject');
	filter = this.plugins('filter');
	sort = this.plugins('angularFilesort');
	util = this.plugins('util');
	bowerFiles = this.plugins('bowerFiles');
	options = {
		starttag: 'files: [',
		endtag: ']',
		addRootSlash: false,
		transform: function(filepath, file, i, length) {
			return '  \'' + filepath + '\'' + (i + 1 < length ? ',' : '');
		}
	};
	appFiles = () => this.src([
	 	'./app/**/*.js',
	 	'./app/partials/**/*'
	]).pipe(this.sort());
	// testFiles = () => this.series(this.bower(), this.mock(), this.app());
	testFiles = () => new Queue({objectMode: true})
		.queue(this.src(this.bowerFiles()).pipe(this.filter('**/*.js')))
      .queue(this.src('./bower_components/angular-mocks/angular-mocks.js'))
      .queue(this.appFiles())
      .queue(this.src('./src/app/**/*_test.js'))
      .done();

	/** Inject all files for tests into karma.conf.js
	 * to be able to run `karma` without gulp. */
	@Task('karma:conf')
	karmaConf() {
		return this.src('./karma.conf.js')
	 		.pipe(this.inject(this.testFiles(), this.options))
      	.pipe(this.dest('./'));
	}
	@Task('karma:run')
	karmaRun(done: Function) {
		let p = path.join(process.cwd(), 'karma.conf.js')
		let log = this.util.log;
		log('karma run path', p);
		let server = new (require('karma').Server)({
		 	//  captureConsole: false,
		 	configFile: p,
		 	singleRun: true
		// });
		}, cb); //.start();
		server.on('browser_error', function (browser, err) {
			log('Karma Run Failed: ' + err.message);
			throw err;
      });
		/** NOTE
		 * it does the job to but returns to gulp to quickly */
		// server.on('run_complete', function (browsers, results) {
		// 	if (results.failed) throw new Error('Karma: Tests Failed');
		// 	log('Karma Run Complete: No Failures');
		// 	done();
		// });
		server.start();
		/** FIXME
		 * !! hack for killing karma after run complete
		 * Any better way to do that ? */
		function cb(exitCode) {
			if (exitCode == 0) {
				log('Karma Run Complete: No Failures');
				done();
			} else {
				log('Karma exited with ', exitCode);
				// process.exit(exitCode)
			}
			log('but the process will never exit...');
			process.exit(exitCode);
		}
	}
	// @Task('karma:watch')
	// karmaWatch() {
	// 	return this.pipe('./.tmp/css');
	// }
}
