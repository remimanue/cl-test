import {Gulpclass, Task} from 'gulpclass';
import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class GulpNGAnnotate extends Pipes.Gulp {
	sources = [
		'./src/app/**/*.js',
		'!./src/app/**/*_test.js', // old stuff for compatibility
		'!./src/app/**/*.test.js', // new stuff
		// './.tmp/partials/**/*.js',
		// './.tmp/partials/' + this.name + '-templates.js'
	];
	options = {
		// remove: true,
      // add: true, // default
      single_quotes: true
	};
	dev = {
		src: this.sources,
		dest: './app'
	};
	prod = {
		src: this.sources,
		dest: './dist/scripts'
	};
	ngAnnotate = (cfg) => this.pipes()
		.pipe(this.plugins('ngAnnotate'), this.options)
		// 	.pipe(this.rename({ extname: '.annotated.js' }))
	 	.pipe(this.dest, cfg.dest)()

	@Task('ng:annotate:dev')
	ngAnnotateDev() {
		return this.src(this.dev.src)
		// .pipe(this.cached('built-css'))
			.pipe(this.ngAnnotate(this.dev))
			// .pipe(this.reload())
			// .pipe(this.debug('Reloaded'));
	}
	@Task('ng:annotate:prod')
	ngAnnotateProd() {
		return this.src(this.prod.src)
			.pipe(this.ngAnnotate(this.prod));
	}
}
