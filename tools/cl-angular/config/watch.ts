import {Gulpclass, Task} from 'gulpclass';
import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class GulpWatch extends Pipes.Gulp {
	util = this.plugins('util');

	change = {
		log: (evt) => this.util.log('log evt', evt),
		reload: (evt) => {
			this.util.log('reload evt', evt);
			if (evt.type !== 'changed') { this.launch('link:dev'); }
		}
	};
	assets = {
		styles: {
			src: './src/styles/**/_*.s+(a|c)ss',
			tasks: ['assets:styles:dev'],
			onChange: this.change.log
		}
	};
	views = {
		src: './src/app/index.html',
		tasks: ['link:dev'],
		// tasks: (evt) => this.sequence(['link:dev']),
		onChange: this.change.log
	};
	partials = {
		src: [
			'./src/app/**/*.html',
			'!' + this.views.src
		],
		tasks: ['partials:dev'],
		onChange: this.change.reload
	};
	scripts = {
		src: './src/app/**/*.js',
		tasks: ['scripts:dev'],
		onChange: this.change.reload
	};
	styles = {
		src: [
			'./src/app/**/*.s+(a|c)ss',
			'!' + this.assets.styles.src
		],
		tasks: ['styles:dev'],
		onChange: this.change.reload
	};

	@Task('watch:assets:sass')
	watchAssetsSass(done: Function) {
		this.watch(this.assets.styles);
	  	done();
	}
	@Task('watch:partials:html')
	watchPartialsHtml(done: Function) {
		this.watch(this.partials);
	  	done();
	}
	@Task('watch:scripts:js')
	watchScriptsJs(done: Function) {
		this.watch(this.scripts);
	  	done();
	}
	@Task('watch:styles:sass')
	watchStylesSass(done: Function) {
		this.watch(this.styles);
		done();
	}
	@Task('watch:views:html')
	watchViewsHtml(done: Function) {
		// let onChange2 = (evt) => {
		// 	this.util.log('evt 2', evt);
		// 	this.sequence('link:dev')
		// 	// this.start('link:dev');
		// };
		// let onAdd = (evt) => {
		// 	this.util.log('evt onAdd', evt);
		// 	// this.start('link:dev');
		// };
		// let onChange3 = (evt) => {
		// 	this.util.log('evt change', evt);
		// 	// this.start('link:dev');
		// };
		// let onUnlink = (evt) => {
		// 	this.util.log('evt unlink', evt);
		// 	// this.start('link:dev');
		// };
		// let onAddDir = (evt) => {
		// 	this.util.log('evt addDir', evt);
		// 	// this.start('link:dev');
		// };
		// let onUnlinkDir = (evt) => {
		// 	this.util.log('evt unlinkDir', evt);
		// 	// this.start('link:dev');
		// };
		// let onError = (evt) => {
		// 	this.util.log('evt error', evt);
		// 	// this.start('link:dev');
		// };
		// let onReady = (evt) => {
		// 	this.util.log('evt ready', evt);
		// 	// this.start('link:dev');
		// };
		// let onRaw = (evt) => {
		// 	this.util.log('evt raw', evt);
		// 	// this.start('link:dev');
		// };
	  	// Initiate livereload server: TODO metrre dans une tache 'server:reload'
		//   	this.reload({ start: true });
		this.watch(this.views);
		// this.watch2('./src/app/index.html', onChange2)
		// .on('add', onAdd)
		// .on('change', onChange3)
		// .on('unlink', onUnlink)
		// .on('addDir', onAddDir)
		// .on('unlinkDir', onUnlinkDir)
		// .on('error', onError)
		// .on('ready', onReady)
		// .on('raw', onRaw);
	  	done();
	}
}
