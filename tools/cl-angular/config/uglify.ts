import {Gulpclass, Task} from 'gulpclass';
import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class GulpUglify extends Pipes.Gulp {
	prod = {
		partials: {
			src: './dist/partials/**/*.js',
			dest: './dist/partials'
		},
		scripts: {
			src: './dist/scripts/**/*.js',
			dest: './dist/scripts'
		}
	};
	partials = {
		src: './dist/' + this.name + '-templates.js',
		dest: './dist'
	};
	scripts = {
		src: './dist/' + this.name + '.js',
		dest: './dist'
	};
	uglify = (cfg) => this.pipes()
		.pipe(this.plugins('uglify'), cfg.opts)
		.pipe(this.rename, { extname: '.min.js' })
		.pipe(this.dest, cfg.dest)();

	@Task('uglify:partials:prod')
	uglifyPartialsProd() {
		return this.src(this.prod.partials.src)
			.pipe(this.uglify(this.prod.partials));
 	}
	@Task('uglify:partials')
	uglifyPartials() {
		return this.src(this.partials.src)
			.pipe(this.uglify(this.partials));
 	}
	@Task('uglify:scripts:prod')
	uglifyScriptsProd() {
		return this.src(this.prod.scripts.src)
			.pipe(this.uglify(this.prod.scripts));
 	}
	@Task('uglify:scripts')
	uglifyScripts() {
		return this.src(this.scripts.src)
			.pipe(this.uglify(this.scripts));
 	}
}
