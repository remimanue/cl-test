import {Gulpclass, Task} from 'gulpclass';
import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class GulpCSSNano extends Pipes.Gulp {
	prod = {
		src: './dist/styles/**/*.css',
		dest: './dist/styles'
	};
	styles = {
		src: './dist/*.css',
		dest: './dist'
	};
	cssnano = (cfg) => this.pipes()
		.pipe(this.plugins('cssnano'))
		.pipe(this.rename, { extname: '.min.css' })
		.pipe(this.dest, cfg.dest)();

	@Task('css:nano:prod')
	cssNanoProd() {
		return this.src(this.prod.src)
			.pipe(this.cssnano(this.prod));
 	}
	@Task('css:nano:styles')
	cssNanoStyles() {
		return this.src(this.styles.src)
			.pipe(this.cssnano(this.styles));
 	}
}
