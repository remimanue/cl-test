import {Gulpclass, Task} from 'gulpclass';
import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class Gulpfile extends Pipes.Gulp {
	options = './.htmlhintrc';
	format = 'htmlhint-stylish';

	partials = {
		src: ['./src/app/**/*.html', '!./src/app/*.html'],
		opts: { 'doctype-first': false },
		format: this.format
	};
	views = {
		src: './src/app/*.html',
		opts: this.options,
		format: this.format
	};

	htmlHint = (cfg) => this.pipes()
    	.pipe(this.plugins('htmlhint'), cfg.opts)
		.pipe(this.plugins('htmlhint').reporter, cfg.format)();
		// .pipe(this.htmlhint.failReporter());

	@Task('html:hint:partials')
	htmlHintPartials() {
		return this.src(this.partials.src)
			.pipe(this.htmlHint(this.partials));
	}
	@Task('html:hint:views')
	htmlHintViews() {
		return this.src(this.views.src)
			.pipe(this.htmlHint(this.views));
	}
	// @Task('html:hint')
	// htmlHint() { return this.pipe(this.sources); }
}
