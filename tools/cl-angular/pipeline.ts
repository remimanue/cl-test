import * as fs from 'fs';
import * as path from 'path';
import * as logger from '../logger';
import * as CFG from '../config';

const gulp = CFG.GULP || require('gulp');
const g = require('gulp-load-plugins')({
	lazy: false,
	pattern: ['gulp-*', 'merge-*', 'run-*', 'main-*', 'event-*'], // the glob to search for
	replaceString: /\bgulp[\-.]|run[\-.]|merge[\-.]|main[\-.]|event[\-.]/, // what to remove from the name of the module when adding it to the context
});
const lazypipe = require('lazypipe');
const DEFAULT = (msg) => lazypipe().pipe(g.debug, { title: msg });

function tryFormatter(name: string) {
	try { return require('./' + path.join('formatters', name)); }
	catch (e) {
		// logger.debug('Pas de formatter pour', name);
		return false;
	}
}
function loadPlugin(name: string) {
	let plugin = g[name];
	if (!plugin) return DEFAULT('Pas de plugin pour ' + name);
	let formatter = tryFormatter(name);
	if (formatter) plugin.addFormatter(formatter);
	return plugin;
}

/** TODO
- Faire un truc pour appeler directement
this.format('csslint')
this.format('htmlhint')
this.format('jshint')
this.format('sass-lint')
*/
import {Gulpclass} from 'gulpclass';

export const getGulp = () => gulp;

@Gulpclass(gulp)
export abstract class Gulp {
	settings = CFG.SETTING;
	name = CFG.NAME;

	src = gulp.src;
	dest = gulp.dest;
	launch = g.sequence;
	watch = (cfg) => gulp.watch(cfg.src, cfg.tasks)
		.on('change', cfg.onChange);
	plugins = loadPlugin;
	pipes = lazypipe;
	debug = (msg) => DEFAULT(msg)();
	cached = (src, name) => this.src(src)
		.pipe(this.plugins('cached')(name));
	reload = (opts?) => lazypipe()
		.pipe(this.plugins('livereload'), opts || {})();
	rename = (opts) => lazypipe()
		.pipe(this.plugins('rename'), opts)();
	sort = (name: string) => lazypipe()
		.pipe(this.plugins(name))();
	formatters = (name: string) => tryFormatter(name) || undefined;
	// log = () => { for (let key in this) console.log('key', key, this[key]); }
}
