/** Scripts */
import {Gulpclass, SequenceTask, Task} from 'gulpclass';
import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class Gulpfile {
	@SequenceTask('scripts:dev')
	scriptsDev() { return [
		'ng:annotate:dev',
		'js:hint:scripts:dev',
	]; }
	@SequenceTask('scripts:prod')
	scriptsProd() { return [
		'ng:annotate:prod',
		'js:hint:scripts:prod',
		'uglify:scripts:prod',
		'concat:scripts',
		'uglify:scripts'
	]; }
}
