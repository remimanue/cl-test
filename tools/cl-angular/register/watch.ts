/** Watch */
import {Gulpclass, SequenceTask, Task} from 'gulpclass';
import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class Gulpfile {
	@SequenceTask('watch:dev')
	watchDev() { return [
		'watch:assets:sass',
		'watch:views:html',
		'watch:styles:sass',
		'watch:scripts:js',
		'watch:partials:html'
	]; }
	/** TODO Sequence */
	@Task('watch:prod')
	watchProd(done: Function) { done(); }
}
