/** Link */
import {Gulpclass, SequenceTask} from 'gulpclass';
import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class Gulpfile {
	@SequenceTask('link:dev')
	linkDev() { return ['inject:dev']; }
	@SequenceTask('link:prod')
	linkProd() { return ['inject:prod']; }
	@SequenceTask('link:test')
	linkTest() { return ['karma:conf']; }
}
