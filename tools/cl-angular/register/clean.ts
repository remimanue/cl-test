import {Gulpclass, SequenceTask, Task} from 'gulpclass';
import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class Gulpfile {
	@SequenceTask('clean:dev')
	cleanDev() { return [
		'clean:vendor:dev',
		'clean:app:dev'
	]; }
	@SequenceTask('clean:prod')
	cleanProd() { return [
		'clean:vendor:prod',
		'clean:app:prod'
	]; }
}
