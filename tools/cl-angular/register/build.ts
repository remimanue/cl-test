import {Gulpclass, SequenceTask, Task} from 'gulpclass';
import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class Gulpfile {
	/** Build */
	@SequenceTask('build:dev')
	buildDev() { return [
		'clean:dev',
		'vendor:dev',
		'styles:dev',
		'partials:dev',
		'scripts:dev'
		/** TODO */
		// 'assets:dev',
	]; }
	/** Dist */
	@SequenceTask('build:prod')
	buildProd() { return [
		'clean:prod',
		'vendor:prod',
		'styles:prod',
		'partials:prod',
		'scripts:prod'
		/** TODO */
		// 'assets:prod',
	]; }
	/** Test */
	@SequenceTask('build:test')
	buildTest() { return [
		'clean:dev',
		'vendor:dev',
		'styles:dev',
		'partials:dev',
		'scripts:dev'
		/** TODO */
		// 'assets:prod',
	]; }
}
