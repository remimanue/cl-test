/** Partials */
import {Gulpclass, SequenceTask, Task} from 'gulpclass';

import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class Gulpfile {
	@SequenceTask('partials:dev')
	partialsDev() { return [
		'html:hint:partials',
		'ng:html:2js:dev',
		// 'ng:annotate:partials:dev',
		'js:hint:partials:dev'
	]; }
	@SequenceTask('partials:prod')
	partialsProd() { return [
		'html:hint:partials',
		'html:min:partials',
		'ng:html:2js:prod',
		'js:hint:partials:prod',
		'uglify:partials:prod',
		'concat:partials',
		'uglify:partials',
	]; }
}
