/** Styles */
import {Gulpclass, SequenceTask} from 'gulpclass';
import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class Gulpfile {
	@SequenceTask('serve:dev')
	serveDev() { return [
		'serve:app'
	]; }
	@SequenceTask('serve:prod')
	serveProd() { return [
		'serve:dist'
	]; }
	/* TODO */
	// @SequenceTask('serve:test')
	// serveTest() { return [
	// 	'karma:start'
	// ]; }
}
