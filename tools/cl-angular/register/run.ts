import {Gulpclass, SequenceTask} from 'gulpclass';
import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class Gulpfile {
	@SequenceTask('run:test')
	runTest() { return ['karma:run']; }
}
