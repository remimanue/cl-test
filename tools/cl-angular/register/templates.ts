/** Templates
 * TODO compile ejs to js
 */
import {Gulpclass, SequenceTask, Task} from 'gulpclass';
import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class Gulpfile {
	@SequenceTask('templates:dev')
	templatesDev() { return [
		'html:hint',
		// 'html:2js:dev'
	]; }
	@SequenceTask('templates;prod')
	templatesProd() { return [
		'html:hint',
		'html:min',
		// 'html:2js:prod',
		'concat:partials'
	]; }
}
