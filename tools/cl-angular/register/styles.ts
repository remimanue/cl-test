/** Styles */
import {Gulpclass, SequenceTask} from 'gulpclass';
import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class Gulpfile {
	@SequenceTask('assets:styles:dev')
	stylesForce() { return [
		'sass:lint:assets',
		'sass:force',
		'css:lint:dev'
	]; }
	@SequenceTask('styles:dev')
	stylesDev() { return [
		'sass:lint:assets',
		'sass:lint:dev',
		'sass:dev',
		'css:lint:dev'
	]; }
	@SequenceTask('styles:prod')
	stylesProd() { return [
		'sass:lint:assets',
		'sass:lint:prod',
		'sass:prod',
		'css:lint:prod',
		'css:nano:prod',
		'concat:styles',
		'css:nano:styles'
	]; }
}
