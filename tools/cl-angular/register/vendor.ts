/** Vendors */
import {Gulpclass, SequenceTask, Task} from 'gulpclass';
import * as Pipes from '../pipeline';

@Gulpclass(Pipes.getGulp())
export class Gulpfile {
	@SequenceTask('vendor:dev')
	vendorDev() { return [
		'bower:fonts:dev',
		'bower:styles:dev',
		'bower:scripts:dev',
	]; }
	@SequenceTask('vendor:prod')
	vendorProd() { return [
		'bower:fonts:prod',
		'bower:styles:prod',
		'bower:scripts:prod',
	]; }
}
