

import * as path from 'path';

const logSymbols = require('log-symbols');
const pluralize = require('pluralize');
const chalk = require('chalk');
const textTable = require('text-table');

let Formatter = {
	id: 'stylish',
	name: 'CSSLint Stylish format',
	totalErrors: 0,
	totalWarnings: 0,
	startFormat: function startFormat() {
	  	this.totalErrors = 0;
	  	this.totalWarnings = 0;
	  	return '';
	},
	endFormat: function endFormat() {
	  	let line = (sym, str, n) =>
	  		'\n[' + logSymbols[sym] + '] ' + pluralize(str, n, true);
	  	let fmt = (str, n) => line(str, str, n);
	  	let output = '';
	  	if (this.totalErrors > 0) output += fmt('error', this.totalErrors);
	  	if (this.totalWarnings > 0) output += fmt('warning', this.totalWarnings);
	  	let viols = this.totalErrors + this.totalWarnings;
	  	output += line(viols === 0 ? 'success' : 'info', 'violation', viols);
	  	return output + '\n';
	},
	formatResults: function formatResults(results, filename, options) {
	  	let _this = this;
	  	let fname = filename;
	  	let messages = results.messages;
	  	let output = [];
	  	let underlinedFilename = undefined;
	  	let _ref = options || {};
	  	if (messages.length > 0) {
		 	if (filename) {
			 	if (!_ref.absoluteFilePathsForFormatters) {
			 		fname = path.relative(process.cwd(), filename);
				}
				underlinedFilename = chalk.underline(fname);
		 	}
		 	messages.forEach(function (origMessage) {
				var message = origMessage.message;
				var rollup = origMessage.rollup;
				var line = origMessage.line;
				var col = origMessage.col;
				var type = origMessage.type;
				let ruleId = origMessage.rule.id;
				let ruleName = origMessage.rule.name;
				let ruleDesc = origMessage.rule.desc;
				let rule = ruleName + ' ' + chalk.dim(ruleId);
				var formatted = [''];
				var isWarning = type === 'warning';
				if (isWarning) {
			  		_this.totalWarnings++;
				} else {
			  		_this.totalErrors++;
				}
				if (rollup) {
			  		formatted.push('');
			  		formatted.push('');
				} else {
					// console.log('chalk', chalk);
					let fmt = (str, n) => chalk.dim(str) + ' ' + chalk.white(n);
			  		formatted.push(fmt('line', line));
			  		formatted.push(fmt('char', col));
					let prefix = (str, color) => logSymbols[str] + '  ' + chalk[color](str);
					formatted.push(isWarning ? prefix('warning', 'yellow') : prefix('error', 'red'));
					formatted.push(' ' + rule + '\n');
				}
				formatted.push(isWarning ? chalk.yellow(message) : chalk.red(message));
				output.push(formatted);
		 	});
	  	}
	  	let result = '\n';
	  	if (underlinedFilename) result += underlinedFilename + '\n';
	  	result += textTable(output);
	  	return result;
	}
}

export = Formatter;
