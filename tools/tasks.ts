
import {Gulpfile} from './gulpfile';

export const config = (name: string) => {
	let gulpfile = new Gulpfile();
	gulpfile.config(name);
	gulpfile.register(name);
};
