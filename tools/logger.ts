// const gulpDebug = require('gulp-debug');

export function start(msg: string) { console.log('START ' + msg); }
export function debug(msg: string, e: any) { console.log('DEBUG ' + msg, e); }
export function warn(msg: string, e: any) { console.log('WARNING ' + msg, e); }
export function error(msg: string, e: any) { console.log('ERROR ' + msg, e); }
export function end(msg: string) { console.log('END ' + msg); }
// export function onStream(msg: string) { return debug({ title: msg }); }
// export function onDebug(msg: string) { return gulpDebug({ title: msg }); }
// export function onError(msg: string, done?: Function) {
// 	return function(e) {
// 		error(msg, e);
// 		done && done();
// 	};
// }
// export function onEnd(msg: string, done?: Function) {
// 	return function() {
// 		end(msg);
// 		done && done();
// 	};
// }
